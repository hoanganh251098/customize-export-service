const
    puppeteer   = require('puppeteer'),
    request     = require('request'),
    express     = require('express'),
    axios       = require('axios'),
    fs          = require('fs')
    ;
let exportDataMap = {};
let resizeDimensions = {
    width: null,
    height: null
};
const app = express();
app.use(express.json());

const printMessage = console.log;

function setResolutionInput(width, height) {
    resizeDimensions.width = width;
    resizeDimensions.height = height;
}

async function detect(previewImgURL) {
    const orderData = exportDataMap[previewImgURL];
    if(!orderData) return;
    const sku = orderData.productName;
    const productDataURL = `https://dev.goodsmize.com/design-upload-v2/products/${sku}/data.json`;
    printMessage('Detecting ...');
    const productData = await new Promise(resolve => {
        request(productDataURL, (err, response, body) => {
            resolve(JSON.parse(body));
        });
    });
    if(sku.includes('canvas')) {
        if(productData.document.width > productData.document.height) {
            setResolutionInput(7500, 5000);
            printMessage('Detect result: hcanvas 7500x5000');
            return;
        }
        else {
            setResolutionInput(5000, 7500);
            printMessage('Detect result: hcanvas 5000x7500');
            return;
        }
    }
    setResolutionInput('', '');
    printMessage('Detect result: keep resolution');
}

async function runExport(previewImgURL, callback) {
    let browser,
        page,
        hashCode,
        jsonData,
        productURL
        ;

    try {
        hashCode = previewImgURL.match(/(?<=products-images\/)[a-zA-Z0-9]+/g)[0];
    }
    catch(e) {
        callback('Failed to get hash code');
        return;
    }

    try {
        printMessage('Loading user input ...');
        jsonData = await (new Promise((resolve, reject) => {
            request(`https://dev.goodsmize.com/design-upload-v2/item-exports/${hashCode}`, function(error, response, body) {
                if(error) reject(error);
                resolve(JSON.parse(body));
            });
        }));
        exportDataMap[previewImgURL] = jsonData;

        detect(previewImgURL);

        printMessage('Loading product page ...');
        productURL  = `https://d1vyizgcxsj7up.cloudfront.net/customize/index.html?product=${jsonData.productName}&mode=debug`;
        browser     = await puppeteer.launch({
            headless: true,
            defaultViewport: null,
            args: ['--no-sandbox'],
            executablePath: '/usr/bin/chromium-browser'
        });
        page        = await browser.newPage();

        try {
            await page.goto(productURL, {timeout: 60000, waitUntil: 'networkidle2'});
        }
        catch(e) {
            await page.goto(productURL, {timeout: 60000, waitUntil: 'networkidle2'});
        }

        printMessage('Fetching user input ...');
        await page.evaluate(async (_) => {
            await App.canvas.import(_);
        }, jsonData);

        await new Promise(resolve => {
            printMessage('Waiting for background ...');
            async function timeoutHandle() {
                const isBgReady = await page.evaluate(async (_) => {
                    let pass = true;
                    App.layers.forEach(layer => {
                        if(!pass) return;
                        if(layer.type == 'fg') {
                            if(!layer.allObjects[0] || !layer.allObjects[1]) pass = false;
                        }
                    });
                    return pass;
                });
                if(isBgReady) {
                    printMessage('Background ready')
                    return resolve();
                }
                setTimeout(timeoutHandle, 1000);
            }
            setTimeout(timeoutHandle, 1000);
        });

        const resizeWidth = resizeDimensions.width;
        const resizeHeight = resizeDimensions.height;

        await new Promise((resolve, reject) => {
            setTimeout(async () => {
                try {
                    const timeoutTimeoutID = setTimeout(async () => {
                        await browser.close();
                        callback('Upload timeout');
                        resolve();
                    }, 300000);
    
                    printMessage('Uploading ...');

                    const uploadData = await page.evaluate(async (_) => {
                        try {
                            const exportedBase64    = await App.canvas.exportBase64();
                            return await App.helper.uploadBlobAWS(App.helper.toBlob(exportedBase64));
                            // if(_.resizeWidth && _.resizeHeight) {
                            //     return await App.helper.uploadBlobAWS(App.helper.imgResizeToBlob(exportedBase64, { width: +_.resizeWidth, height: +_.resizeHeight }));
                            // }
                            // else {
                            //     return await App.helper.uploadBlobAWS(App.helper.toBlob(exportedBase64));
                            // }
                        }
                        catch(e) {
                            return { error: e };
                        }
                    }, { resizeWidth, resizeHeight });
                    clearTimeout(timeoutTimeoutID);

                    if(uploadData.error) {
                        throw uploadData.error;
                    }
            
                    await browser.close();

                    printMessage('Done.');

                    callback(null, uploadData.data);

                    resolve();
                }
                catch(e) {
                    console.log(e);
                    callback(e);
                }
            }, 300000);
        });
    }
    catch(e) {
        await browser.close();
        callback(`Error: ${JSON.stringify(e)}`);
    }
}

function updateOrderMetafield({orderID, key, value}) {
    // const baseURL = 'https://c7c2ececa8bea44f97b2ee55b13997ea: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@coupleprintsateam.myshopify.com';
    // const orderMetafieldsURL = baseURL + `/admin/orders/${orderID}/metafields.json`;
    // axios.post(orderMetafieldsURL, {
    //     metafield: {
    //         namespace: 'TAT_customize',
    //         key: key,
    //         value: value,
    //         value_type: 'string'
    //     }
    // })
    // .catch(err => {
    //     console.log('Failed to set metafield');
    //     console.log(err);
    //     const logMessage = `\n${(new Date).toLocaleString()}\nFailed to set metafield:\n${JSON.stringify(err)}\n`;
    //     fs.appendFileSync('log.txt', logMessage);
    // });
    const baseURL = 'https://c7c2ececa8bea44f97b2ee55b13997ea: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@coupleprintsateam.myshopify.com';
    const orderMetafieldsURL = baseURL + `/admin/api/2021-04/orders/${orderID}.json`;
    axios.put(orderMetafieldsURL, {
        order: {
            id: orderID,
            note: `Quy trình tự động chưa ổn định\nSo sánh với ảnh preview để tránh sai sót khi fulfill\n${JSON.parse(value).map((url, idx) => `Item #${idx+1} print image:\n${url}`).join('\n')}`
        }
    })
    .catch(err => {
        console.log('Failed to set metafield');
        console.log(err);
        const logMessage = `\n${(new Date).toLocaleString()}\nFailed to set metafield:\n${JSON.stringify(err)}\n`;
        fs.appendFileSync('log.txt', logMessage);
    });
}

app.get('/webhook/customize-export/log', (req, res) => {
    const html = `
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Log</title>
        <script src="/webhook/customize-export/log/script.js?v=${+ new Date}" defer></script>
    </head>
    <body style="background:#d1d1d1">
        <textarea spellcheck="false" style="width:calc(100% - 5px);height:80vh;outline: none;"></textarea>
        <button>Clear log</button>
    </body>
    </html>
    `;
    res.send(html);
});

app.get('/webhook/customize-export/log/script.js', (req, res) => {
    const script = `
    const data = {message:${JSON.stringify(fs.readFileSync('log.txt').toString())}};
    document.querySelector('textarea').value = data.message;
    document.querySelector('button').onclick = function() {
        fetch('/webhook/customize-export/clear-log', {
            method: 'post'
        }).then(function(response) {
            location.reload();
        });
    };
    `;
    res.send(script);
});

app.post('/webhook/customize-export/clear-log', (req, res) => {
    fs.writeFileSync('log.txt', '');
    res.sendStatus(200);
});

app.post('/webhook/customize-export', (req, res) => {
    const orderData = req.body;
    const customerEmail = orderData.customer.email;
    const customerName = orderData.customer.first_name + ' ' + orderData.customer.last_name;
    const exportImages = Array(orderData.line_items.length).fill(null);
    fs.appendFileSync('log.txt', `\nExporting order ${orderData.id}\n`);
    for(const [idx, lineItem] of orderData.line_items.entries()) {
        const linkImageProperty = lineItem.properties.filter(prop => prop.name == 'link_image')[0];
        if(!linkImageProperty) continue;
        const previewImgURL = linkImageProperty.value;
        runExport(previewImgURL, (err, result) => {
            if(err) {
                const logMessage = `\n${(new Date).toLocaleString()}\n${err}\n`;
                fs.appendFileSync('log.txt', logMessage);
                return;
            }
            exportImages[idx] = result;
            updateOrderMetafield({
                orderID: orderData.id,
                key: 'Design_Download',
                value: JSON.stringify(exportImages)
            });
            if(result.includes('http')) {
                const itemIdx = idx + 1;
                axios.post('https://dev.goodsmize.com/sender_email', {
                    receiver_email: customerEmail,
                    subject: 'CouplePrints fulfillment service',
                    content: `Hi ${customerName}, <br>Thank you for believing and choosing CouplePrints. <br>Here is the digital file of your stunning design. We hope you enjoy it! <br>Kind regards,<br>CouplePrints<br><a href='${result}'>${result}</a>`
                });
            }
        });
    }
    res.sendStatus(200);
});

app.get('/webhook/customize-export/manual', (req, res) => {
    const orderID = req.query.orderID;
    const baseURL = 'https://c7c2ececa8bea44f97b2ee55b13997ea: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@coupleprintsateam.myshopify.com';
    const orderDataURL = baseURL + `/admin/api/2021-04/orders/${orderID}.json`;
    axios.get(orderDataURL)
    .then(response => {
        const orderData = response.data.order;
        const exportImages = Array(orderData.line_items.length).fill(null);
        for(const [idx, lineItem] of orderData.line_items.entries()) {
            const linkImageProperty = lineItem.properties.filter(prop => prop.name == 'link_image')[0];
            if(!linkImageProperty) continue;
            const previewImgURL = linkImageProperty.value;
            runExport(previewImgURL, (err, result) => {
                if(err) {
                    const logMessage = `\n${(new Date).toLocaleString()}\n${err}\n`;
                    fs.appendFileSync('log.txt', logMessage);
                    return;
                }
                exportImages[idx] = result;
                updateOrderMetafield({
                    orderID: orderData.id,
                    key: 'Design_Download',
                    value: JSON.stringify(exportImages)
                });
            });
        }
        res.send('ok');
    });
});

app.listen(3000);
